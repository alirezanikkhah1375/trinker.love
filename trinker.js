module.exports = {
    title: function () {
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function (title = "=") {
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    // finding ALL men . . .
    allMale: function (p) {
        return p.filter(index => (index.gender == "Male"));
    },

    // finding ALL women . . .
    allFemale: function (p) {
        return p.filter(index => (index.gender == "Female"));
    },

    // finding number of MEN . . . 
    nbOfMale: function (p) {
        return this.allMale(p).length;
    },

    // finding number of WOMEN . . . 
    nbOfFemale: function (p) {
        return this.allFemale(p).length;
    },

    // WOMEN interested in men . . .  
    allMaleInterest: function (p) {
        return p.filter(index => (index.looking_for.includes("M")));
    },

    nbOfMaleInterest: function (p) {
        return this.allMaleInterest(p).length;
    },

    // MEN interested in women . . .  
    allFemaleIntereset: function (p) {
        return p.filter(index => (index.looking_for.includes("F")));
    },

    nbOfFemaleInterest: function (p) {
        return this.allFemaleIntereset(p).length;
    },

    richPeople: function (p) {
        return p.filter(index => (parseFloat(index.income.substring(1)) > 2000)).length;
    },

    dramaFans: function (p) {
        return p.filter(index => (index.pref_movie.includes("Drama"))).length;
    },

    sciFiWomen: function (p) {
        return p.filter(index => (index.gender == "Female" && index.pref_movie.includes("Sci-Fi"))).length;
    },

    documentary1500: function (p) {
        return p.filter(index => parseFloat(index.income.substring(1)) > 1482 && index.pref_movie.includes("Documentary")).length;
    },

    peoplePlus4k: function (p) {
        return p.filter(index => parseFloat(index.income.substring(1)) > 4000);
    },

    infoPeoplePlus4k: function (p) {
        return this.peoplePlus4k(p).map(index => (` ${index.first_name} ${index.last_name} with the id ${index.id} gain ${index.income} `));
    },

    /* Sorting Part . . . 

    sortingToLeast: function (p) {
        return p.sort((a, b) => a - b);
    },

    sortingToMost: function (p) {
        return p.sort((a, b) => b - a);
    },

    */

    // sorting MEN by their income (richest to poorest) . . . 
    richestManTable: function (p) {
        return this.allMale(p).sort((a, b) => parseFloat(b.income.substring(1)) - parseFloat(a.income.substring(1)))[0];
    },

    // editing elements in richest man's table . . . 
    richestManEdited: function (p) {
        const rich = this.richestManTable(p)
        return ` ${rich.first_name} ${rich.last_name} with the salary ${rich.income} and the id of ${rich.id} is the richest man of all. `;
    },

    averageSalary: function (p) {
        let total = 0;
        p.forEach(salary => {
            const salaries = (parseFloat(salary.income.substring(1)));
            total += salaries;
        });
        let average = (total / p.length).toFixed(2);
        return `${average} $`;
    },

    medianSalary: function (p) {

        const soretedSalaries = p.sort((a, b) => parseFloat(b.income.substring(1)) - (a.income.substring(1)));


        if (soretedSalaries.length % 2 == 0) {

            let first = parseFloat(p[soretedSalaries.length / 2].income.substring(1));
            let second = parseFloat(p[(soretedSalaries.length / 2) - 1].income.substring(1));;
            return ` ${(first + second) / 2} $ `;
        } else {
            return `${parseFloat(p[p.length / 2].income.substring(1))} € `;
        }
    },

    nbOfNord: function (p) {
        return p.filter(index => (index.latitude > 0)).length;
    },

    averageSalaryOfSud: function (p) {
        const southPeople = p.filter(index => (index.latitude < 0));
        let total = 0;
        for (let i = 0; i < southPeople.length; i++) {
            const salary = parseFloat(southPeople[i].income.substring(1));
            total += salary;
        }

        let average = (total / southPeople.length).toFixed(2);
        return `${average} $ `;
    },

    cawtNeighbor: function (p) {

        const cawt = p.find(index => index.last_name == "Cawt"); // Ok
        const withoutHim = p.filter(index => index.last_name !== "Cawt"); // Ok
        let personLatitude, personLongitude, distanceResult; // defining variables
        for (let i = 0; i < withoutHim.length; i++) { // for access to each object of list
            personLatitude = withoutHim[i].latitude; // each person's latitude 
            personLongitude = withoutHim[i].longitude; // each person's longitude 
            distanceResult = Math.sqrt(Math.pow((personLatitude - cawt.latitude), 2) + Math.pow((personLongitude - cawt.longitude), 2)).toFixed(2);
            withoutHim[i]["distance"] = distanceResult // distance between cawt and each person
        }
        const distansesToMost = withoutHim.sort((a, b) => (a.distance) - (b.distance)) // sorting them to most distance
        let closestNeighbor = distansesToMost[0] // choosing least distance (closest one)
        return ` Closest neighbor is ${closestNeighbor.first_name} ${closestNeighbor.last_name} with the id ${closestNeighbor.id} and the distance of ${closestNeighbor.distance}`;
    },

    brachNeighbor: function (p) {

        const brach = p.find(index => index.last_name == "Brach"); // Ok
        const withoutHim = p.filter(index => index.last_name !== "Brach"); // Ok
        let personLatitude, personLongitude, distanceResult; // defining variables
        for (let i = 0; i < withoutHim.length; i++) { // for access to each object of list
            personLatitude = withoutHim[i].latitude; // each person's latitude 
            personLongitude = withoutHim[i].longitude; // each person's longitude 
            distanceResult = Math.sqrt(Math.pow((personLatitude - brach.latitude), 2) + Math.pow((personLongitude - brach.longitude), 2)).toFixed(2);
            withoutHim[i]["distance"] = distanceResult // distance between cawt and each person
        }
        const distansesToMost = withoutHim.sort((a, b) => (a.distance) - (b.distance)) // sorting them to most distance
        let closestNeighbor = distansesToMost[0] // choosing least distance (closest one)
        return ` Closest neighbor is ${closestNeighbor.first_name} ${closestNeighbor.last_name} with the id ${closestNeighbor.id} and the distance of ${closestNeighbor.distance}`;
    },

    boshardNeighbor: function (p) {

        const boshard = p.find(index => index.last_name == "Boshard"); // Ok
        const withoutHim = p.filter(index => index.last_name !== "Boshard"); // Ok
        let personLatitude, personLongitude, distanceResult; // defining variables
        for (let i = 0; i < withoutHim.length; i++) { // for access to each object of list
            personLatitude = withoutHim[i].latitude; // each person's latitude 
            personLongitude = withoutHim[i].longitude; // each person's longitude 
            distanceResult = Math.sqrt(Math.pow((personLatitude - boshard.latitude), 2) + Math.pow((personLongitude - boshard.longitude), 2)).toFixed(2);
            withoutHim[i]["distance"] = distanceResult // distance between cawt and each person
        }
        const distansesToMost = withoutHim.sort((a, b) => (a.distance) - (b.distance)) // sorting them to furthest 
        let closestNeighbors = distansesToMost.slice(0, 10); // choosing the first 10 elements by .slice() function
        // let txtNames = []; // method 1
        // for (let i = 0; i < closestNeighbors.length; i++) {
        //     let adding = txtNames.push(` Number ${closestNeighbors[i].id} : ${closestNeighbors[i].first_name} ${closestNeighbors[i].last_name} with the distance ${closestNeighbors[i].distance} `)
        // }
        // return txtNames;

        // method 2
        return closestNeighbors.map(index => ` Number ${index.id} : ${index.first_name} ${index.last_name} with the distance ${index.distance} `)
    },

    googleWorkers: function (p) {
        const googleEmails = p.filter(index => index.email.includes("@google")); // for loop doesn't work because emails include google and they are not equal to google.
        return googleEmails.map(index => ` ${index.first_name} ${index.last_name} with the id of ${index.id} `)
    },

    calculatingAges: function (p) {
        // let ageAdded = p.slice(); // old way of making a copy . . . 
        let ageAddedList = [...p]; // new way . . .

        // calculation for minute , hour ,  day , month , year from millisecond . . . 
        const minute = 1000 * 60;
        const hour = minute * 60;
        const day = hour * 24;
        const year = day * 365;

        // adding ages to the objects of the array 
        for (let i = 0; i < ageAddedList.length; i++) {

            let dateOfBirth = ageAddedList[i].date_of_birth; // getting date of birth
            let bdt = new Date(dateOfBirth); // changing it to date version of js (1)
            let bdt_value = bdt.getTime() // getting milliseconds from 1970 (2)
            let today = new Date() // date of today (1)
            let now = today.getTime() // changing it to milliseconds from 1970 (2)
            let diffrence = now - bdt_value // calculating the difference the ages 
            let age = Math.floor(diffrence / year) // rounding the result and dividing to number of milliseconds in one year

            // adding age to each object of the array
            ageAddedList[i]["age"] = age

        }
        return ageAddedList
    },

    oldestPerson: function (p) {

        const sortingToOldest = this.calculatingAges(p).sort((a, b) => (b.age) - (a.age))
        return ` Oldest person is ${sortingToOldest[1].first_name} ${sortingToOldest[1].last_name} with age of ${sortingToOldest[1].age} years old. `
    },

    youngestPerson: function (p) {

        const sortingToOldest = this.calculatingAges(p).sort((a, b) => (a.age) - (b.age))
        return ` Youngest person is ${sortingToOldest[1].first_name} ${sortingToOldest[1].last_name} with age of ${sortingToOldest[1].age} years old. `
    },

    averageOfAgeDifferences: function (p) { // J'ai pas bien compris la question ...

        const list = this.calculatingAges(p).sort((a, b) => (b.age) - (a.age))
        let total = 0;
        for (let i = 0; i < list.length; i++) {
            let age = list[i].age;
            total -= age
            if (total < 0) {
                total = Math.abs(total);
            };
        }
        return (total / list.length)

    },

    comedyMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Comedy"))).length; // 304
    },

    westernMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Western"))).length; // 33
    },

    romanceMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Romance"))).length;
    },

    dramaMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Drama"))).length;
    },

    actionMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Action"))).length;
    },

    sciFiMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Sci-fi"))).length;
    },

    crimeMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Crime"))).length;
    },

    documentaryMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Documentary"))).length;
    },

    horrorMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Horror"))).length;
    },

    mysteryMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Mystery"))).length;
    },

    thrillerMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Thriller"))).length;
    },

    fantasyMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Fantasy"))).length;
    },

    animationMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Animation"))).length;
    },

    warMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("War"))).length;
    },

    musicalMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Musical"))).length;
    },

    adventureMovies: function (p) {
        return p.filter(index => (index.pref_movie.includes("Adventure"))).length;
    },

    match: function (p) {
        return "not implemented".red;
    },


    // let container = [];
    // for (let i = 0; i < p.length; i++) {
    //     if (p.longitude < 0) {
    //         container.push(p[i]);
    //     };
    // };

}